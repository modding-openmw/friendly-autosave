if require("openmw.core").API_REVISION < 62 then
	error("OpenMW 0.49 or newer is required!")
end

local MOD_ID = "FriendlyAutosave"
local SETTINGS_KEY = "SettingsPlayer" .. MOD_ID

return {
    COMBAT_END_DISTANCE = 7800,
    MOD_ID = MOD_ID,
    SETTINGS_KEY = SETTINGS_KEY
}
