local async = require("openmw.async")
local self = require("openmw.self")
local storage = require("openmw.storage")
local time = require("openmw_aux.time")
local I = require("openmw.interfaces")
local common = require("scripts.friendly-autosave.common")
local playerStorage = storage.playerSection(common.SETTINGS_KEY)

local COMBAT_CHECK_INTERVAL = 1

I.Settings.registerPage {
    key = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "name",
    description = "description"
}

I.Settings.registerGroup {
    key = common.SETTINGS_KEY,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "settingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = "saveInterval",
            name = "saveInterval_name",
            description = "saveInterval_desc",
            default = 15,
            renderer = "number",
            min = 1,
            max = 60
        },
        {
            key = "maxSaves",
            name = "maxSaves_name",
            description = "maxSaves_desc",
            default = 5,
            renderer = "number",
            min = 1,
            max = 100
        },
        {
            key = "fancyFilenames",
            name = "fancyFilenames_name",
            description = "fancyFilenames_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "combatSave",
            name = "combatSave_name",
            description = "combatSave_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "sneakSave",
            name = "sneakSave_name",
            description = "sneakSave_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "solidGroundSave",
            name = "solidGroundSave_name",
            description = "solidGroundSave_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "spellStanceSave",
            name = "spellStanceSave_name",
            description = "spellStanceSave_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "weaponStanceSave",
            name = "weaponStanceSave_name",
            description = "weaponStanceSave_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "debugMsgs",
            name = "debugMsgs_name",
            description = "debugMsgs_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local cooldown = playerStorage:get("saveInterval") * time.minute
local debugMsgs = playerStorage:get("debugMsgs")
local inCombat = {}
local menuOpen = false
local saveFiles = {}
local saveSlot = 0
local total = 0

local function updateCooldown(_, key)
    if key == "saveInterval" then
        cooldown = playerStorage:get("saveInterval") * time.minute
    end
end
playerStorage:subscribe(async:callback(updateCooldown))

local function updateDebugMsgs(_, key)
    if key == "debugMsgs" then
        debugMsgs = playerStorage:get("debugMsgs")
    end
end
playerStorage:subscribe(async:callback(updateDebugMsgs))

local function msg(str)
    if debugMsgs then
        print(string.format("[%s]: %s", common.MOD_ID, str))
    end
end

local function combatCheckCallback()
    for _, actor in pairs(inCombat) do
        if (actor.position - self.position):length() >= common.COMBAT_END_DISTANCE then
            inCombat[actor.id] = nil
            actor:sendEvent("momw_nas_distanceEndCombat")
        end
    end
end
time.runRepeatedly(combatCheckCallback, COMBAT_CHECK_INTERVAL)

local function preSaveChecks()
    if self.type.quests(self)["A1_1_FindSpymaster"].stage < 1 then
        msg("Chargen isn't finished!")
        return false
    end
    if playerStorage:get("solidGroundSave") == true and (self.position.z < 1 or not self.type.isOnGround(self)) then
		msg("Can't save; Player not on solid ground")
        return false
    end
    if playerStorage:get("combatSave") == false and next(inCombat) ~= nil then
		msg("Can't save; Player in combat")
        return false
    end
    if playerStorage:get("spellStanceSave") == false and self.type.getStance(self) == self.type.STANCE.Spell then
		msg("Can't save; Spell stance active")
        return false
    end
    if playerStorage:get("weaponStanceSave") == false and self.type.getStance(self) == self.type.STANCE.Weapon then
		msg("Can't save; Weapon stance active")
        return false
    end
	if playerStorage:get("sneakSave") == false and self.controls.sneak == true then
		msg("Can't save; Player is sneaking")
        return false
	end
    if menuOpen then
		msg("Can't save; Menu is open")
        return false
    end
    return true
end

local function titleCase(first, rest)
    -- THANKS: https://stackoverflow.com/a/22548737
   return first:upper() .. rest:lower()
end

local function onLoad(data)
    if not data then return end
    inCombat = data.inCombat
    saveFiles = data.saveFiles or {}
    saveSlot = data.saveSlot
end

local function onSave()
    return {
        inCombat = inCombat,
        saveFiles = saveFiles,
        saveSlot = saveSlot
    }
end

local function onUpdate(deltaTime)
    local dt = deltaTime or 0
    total = total + dt
    if total < cooldown then return end
    if preSaveChecks() then
        total = 0
        if saveSlot == playerStorage:get("maxSaves") then
            saveSlot = 1
        else
            saveSlot = saveSlot + 1
        end
        local location = self.cell.name
        if location == "" then
            location = string.gsub(self.cell.region, "(%a)([%w_']*)", titleCase)
        end
        self.type.sendMenuEvent(
            self, "momw_fas_saveGame",
            {
                debugMsgs = debugMsgs,
                fancyFilenames = playerStorage:get("fancyFilenames"),
                location = location,
                maxSaves = playerStorage:get("maxSaves"),
                saveFiles = saveFiles,
                slot = saveSlot
            }
        )
    end
end

local function registerCombat(data)
    if data.done == true then
        inCombat[data.entity.id] = nil
    else
        inCombat[data.entity.id] = data.entity
    end
end

local function registerSaveFile(saveFile)
    saveFiles[saveSlot] = saveFile
end

local function UiModeChanged(data)
    if data.newMode == nil then
        menuOpen = false
    else
        menuOpen = true
    end
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave,
        onUpdate = onUpdate,
    },
    eventHandlers = {
        momw_fas_playerRegisterCombat = registerCombat,
        momw_fas_playerRegisterSaveFile = registerSaveFile,
        UiModeChanged = UiModeChanged
    }
}
