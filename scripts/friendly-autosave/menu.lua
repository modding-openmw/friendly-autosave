local core = require("openmw.core")
local menu = require("openmw.menu")
local common = require("scripts.friendly-autosave.common")

local function saveGame(data)
    local saveDir = menu.getCurrentSaveDir()
    local saveName = string.format("%s%s: %s", common.MOD_ID, data.slot, data.location)
    if data.fancyFilenames == false then
        saveName = string.format("%s%s", common.MOD_ID, data.slot)
    end
    local saveFile = (saveName .. ".omwsave"):gsub(":", "_"):gsub("'", "_"):gsub(" ", "_"):gsub(",", "_")
    local toDelete = data.saveFiles[data.slot]
    print(string.format("toDelete: %s", toDelete))
    if saveDir then
        if menu.getSaves(saveDir)[toDelete] then
            if data.debugMsgs then
                print(string.format("[%s]: Deleting existing save file: %s", common.MOD_ID, toDelete))
            end
            menu.deleteGame(saveDir, toDelete)
        end
    end
    if data.debugMsgs then
        print(string.format("[%s]: Saving: %s", common.MOD_ID, saveFile))
    end
    menu.saveGame(saveName, saveName)
    core.sendGlobalEvent("momw_fas_globalRegisterSaveFile", saveFile)
end

return {eventHandlers = {momw_fas_saveGame = saveGame}}
