local world = require('openmw.world')

local function registerCombat(data)
    world.players[1]:sendEvent("momw_fas_playerRegisterCombat", data)
end

local function registerSaveFile(saveFile)
    world.players[1]:sendEvent("momw_fas_playerRegisterSaveFile", saveFile)
end

return {
    eventHandlers = {
        momw_fas_globalRegisterCombat = registerCombat,
        momw_fas_globalRegisterSaveFile = registerSaveFile
    }
}
