# Friendly Autosave

Saves the game at regular intervals with configurable exceptions.

See the script settings menu for all configuration options (ESC >> Options >> Scripts >> Friendly Autosave)

#### Credits

Author: **johnnyhostile**

#### Requirements

OpenMW 0.49 or newer

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/friendly-autosave/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Save\friendly-autosave

        # Linux
        /home/username/games/OpenMWMods/Save/friendly-autosave

        # macOS
        /Users/username/games/OpenMWMods/Save/friendly-autosave

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Save\friendly-autosave"`)
1. Add `content=friendly-autosave.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Configuration

This mod may be configured via the in-game script menu, found at: ESC >> Options >> Scripts >> Friendly Autosave

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/friendly-autosave/-/issues)
* Email `friendly-autosave@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
