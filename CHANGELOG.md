## Friendly Autosave Changelog

#### Version 1.4

* The default save interval is now 15 minutes instead of 5
* New save condition options added:
    * Require solid ground (default: yes)
    * Save when in spell stance (default: no)
    * Save when in weapon stance (default: no)
* Made fancy file names option, off by default

[Download Link](https://gitlab.com/modding-openmw/friendly-autosave/-/packages/25780757)

#### Version 1.3

* Fixed a typo that broke the save interval unless it was manually changed

[Download Link](https://gitlab.com/modding-openmw/friendly-autosave/-/packages/25355158)

#### Version 1.2

* Fixed an issue with deleting saves when the max save amount was reached

[Download Link](https://gitlab.com/modding-openmw/friendly-autosave/-/packages/25354973)

#### Version 1.1

* Improved save descriptions
* Optimized reading saves

[Download Link](https://gitlab.com/modding-openmw/friendly-autosave/-/packages/25354667)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/friendly-autosave/-/packages/25354481)
